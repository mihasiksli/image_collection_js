imageCollectionApp
    .controller('LoaderCtrl', function($scope, $rootScope, $http, ConfigService) {
        $rootScope.dataLoading = true;
        $rootScope.config = ConfigService;
        $rootScope.itmz = [1, 2, 4, 5];
        $http.get('app/data/data.json').then(function(response) {
            $rootScope.dataLoading = false;
            $rootScope.fullImageData = response.data;
            $rootScope.fullImageData.sort(function(a, b) {
                if (parseInt(a.id) < parseInt(b.id)) return -1;
                if (parseInt(a.id) > parseInt(b.id)) return 1;
                return 0;
            });
            angular.forEach($rootScope.fullImageData, function(v, i) {
                v.arrInd = i;
            });
        });
    })
    .controller('MainCtrl', function($scope, $rootScope, $location, $window) {
        $scope.itemNumberOptions = [{val: 20}, {val: 50}];
        $scope.workData = {
            reversed: false,
            itemsOnPage: 20,
            currentPage: undefined,
            numberOfPages: 0,
            pageSubList: [],
            dataSubList: [],
            searchText: ""
        };

        console.log($scope.workData.reversed);

        function setData() {
            if (!$scope.imageData) return;
            $scope.workData.numberOfPages = Math.ceil($scope.imageData.length / $scope.workData.itemsOnPage);
            if ($scope.workData.numberOfPages > 0 && !$scope.workData.currentPage)
                $scope.workData.currentPage = 1;
            else if ($scope.workData.currentPage && $scope.workData.currentPage > $scope.workData.numberOfPages)
                $scope.workData.currentPage = $scope.workData.numberOfPages;
            $scope.workData.pageSubList = getPageSubList();
            $scope.workData.dataSubList = getDataSubList();
        }
        function getPageSubList() {
            var pageList = [];
            var min, max;
            if ($scope.workData.numberOfPages < 10) {
                min = 1;
                max = $scope.workData.numberOfPages;
            }
            else {
                if ($scope.workData.currentPage <= 5) { min = 1; max = 10; }
                else if ($scope.workData.currentPage > $scope.workData.numberOfPages - 5) {
                    min = $scope.workData.numberOfPages - 9; max = $scope.workData.numberOfPages; }
                else {min = $scope.workData.currentPage - 4;max = $scope.workData.currentPage + 5;
                }
            }
            for (var i = min; i <= max; i++) pageList.push(i);
            return pageList;
        }
        function getDataSubList() {
            var begin = ($scope.workData.currentPage - 1) * $scope.workData.itemsOnPage;
            var end = begin + $scope.workData.itemsOnPage;
            if (begin > $scope.imageData.length - 1) begin = $scope.imageData.length - 1;
            if (end > $scope.imageData.length - 1) return $scope.imageData.slice(begin);
            return $scope.imageData.slice(begin, end);
        }

        $scope.setPage = function(p) {
            $scope.workData.currentPage = parseInt(p);
            setData();
        };
        $rootScope.$watch('fullImageData', function() {
            if ($rootScope.fullImageData) $scope.imageData = $rootScope.fullImageData.slice();
        });
        $scope.$watch('imageData', function() { setData(); });
        $scope.$watch('workData.itemsOnPage', function(d) { setData(); });
        $scope.$watch('workData.reversed', function(d) {
            if ($scope.imageData) {
                if (d && $scope.imageData[0].id < $scope.imageData[$scope.imageData.length - 1].id)
                    $scope.imageData.reverse();
                else if (!d && $scope.imageData[0].id > $scope.imageData[$scope.imageData.length - 1].id)
                    $scope.imageData.reverse();
            }
            setData();
        });
        $scope.search = function() {
            if ($scope.workData.searchText == "") {
                $scope.imageData = $rootScope.fullImageData.slice();
            }
            else {
                $scope.imageData = [];
                if ($scope.workData.searchText.length < 3) return;
                var strArray = $scope.workData.searchText.split(" ");
                angular.forEach($rootScope.fullImageData, function(v, i) {
                    if (v.id == $scope.workData.searchText) $scope.imageData.push(v);
                    else {
                        var add = true;
                        angular.forEach(strArray, function(s) {
                            if (v.message.toLowerCase().indexOf(s.toLowerCase()) == -1) add = false;
                        });
                        if (add) $scope.imageData.push(v);
                    }
                });
            }
            if ($scope.workData.reversed) $scope.imageData.reverse();
        };
        $scope.openPost = function(id) {
            $window.open('/#/p/' + id, '_blank');
        }
    })
    .controller('PostCtrl', function($scope, $rootScope, $routeParams, $location) {
        $scope.id = $routeParams.arrId;
        $rootScope.$watch('fullImageData', function() {
            if (!$rootScope.fullImageData) return;
            $scope.postData = $rootScope.fullImageData[$scope.id];
            console.log($scope.postData);
            $scope.photoIndex = 0;
            if (!$scope.postData) $location.path('/');
        });
        $scope.changePhotoIndex = function (index) {
            $scope.photoIndex = index;
        };
    });
