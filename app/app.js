/**
 * Created by mk on 24.06.16.
 */

var imageCollectionApp = angular.module('img_coll', ['ngMaterial', 'ngRoute', 'ngResource']);

imageCollectionApp
    .config(['$routeProvider', '$resourceProvider',
        function($routeProvider, $resourceProvider) {
            $routeProvider.when('/', {
                templateUrl: '/views/main.html',
                controller: 'MainCtrl'
            }).when('/p/:arrId', {
                templateUrl: '/views/post.html',
                controller: 'PostCtrl'
            })
        }]);